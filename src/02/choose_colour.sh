choose_colour() {
    bg_color_val_names=('47' '41' '42' '44' '45' '40')
    font_color_val_names=('97' '91' '92' '94' '95' '90')
    colour_category="\e[${bg_color_val_names[$1 - 1]};${font_color_val_names[$2 - 1]}m"
    colour_data="\e[${bg_color_val_names[$3 - 1]};${font_color_val_names[$4 - 1]}m"
}