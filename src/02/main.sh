#!/bin/bash
source ./get_info.sh
source ./print_info.sh
source ./choose_colour.sh

if [[ $# -ne 4 ]]; then
    echo "You have to enter exactly 4 arguments"
    exit 1
fi
for i in {1..4}; do
    if ! [[ ${!i} =~ ^[1-6]$ ]]; then
        print_error_range
        exit 1
    fi
done

if [ "${1}" = "${2}" ] || [ "${3}" = "${4}" ]; then
    echo "Error: Font and background colors of one column must not match. Please try again."
    read -p "Do you want to run the script again with different parameters? (Y/N) " choice
    if [[ ${choice} =~ ^[Yy]$ ]]; then
        echo "Usage: $0 <bg_color_val_names> <font_color_val_names> <bg_color_vals> <font_color_vals>"
        exit 1
    else
        exit 1
    fi
fi

get_info  # получаем необходимую информацию
choose_colour "$@" # выбираем подходящие цвета
print_info  # выводим цвета

exit 0