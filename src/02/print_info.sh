. ./choose_colour.sh

print_info() {
    NC='\e[0;0m'
for piece in "${INFO[@]}" ; do
    key="${piece%% =*}"
    inf="${piece##*= }"
    echo -e "${colour_category}$key${NC} = ${colour_data}$inf${NC}"
done
}

print_error_range() {
    echo "Error: Parameter color must be a number between 1 and 6."
    echo "       Available colors:"
    echo "       1 - white"
    echo "       2 - red"
    echo "       3 - green"
    echo "       4 - blue"
    echo "       5 - purple"
    echo "       6 - black"
}