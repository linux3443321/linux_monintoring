. ./choose_colour.sh

print_info() {
    NC='\e[0;0m'
for piece in "${INFO[@]}" ; do
    key="${piece%% =*}"
    inf="${piece##*= }"
    echo -e "${colour_category}$key${NC} = ${colour_data}$inf${NC}"
done
}