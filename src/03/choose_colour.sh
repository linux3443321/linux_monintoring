choose_colour() {
    bg_color_val_names=('47' '41' '42' '44' '45' '40')
    font_color_val_names=('97' '91' '92' '94' '95' '90')
    colour_names=('white' 'red' 'green' 'blue' 'purple' 'black')
    first="$(grep column1_b settings.conf | tail -c 2 | head -c 1)"
    second=$(grep column1_f settings.conf | tail -c 2 | head -c 1)
    third=$(grep column2_b settings.conf | tail -c 2 | head -c 1)
    forth=$(grep column2_f settings.conf | tail -c 2 | head -c 1)
    colour_message=()
    if [[ ! $first =~ ^[1-6] ]]; then 
        first="1"  # white
        colour_message+=("Column 1 background = default (white)")
    else
        colour_message+=("Column 1 background = $first (${colour_names[$first - 1]})")
    fi
    if [[ ! $second =~ ^[1-6] ]]; then 
        second="2"  # red
        colour_message+=("Column 1 font colour = default (red))")
    else
        colour_message+=("Column 1 font colour = $second (${colour_names[$second - 1]})")
    fi
    if [[ ! $third =~ ^[1-6] ]]; then 
        third="1"  # white
        colour_message+=("Column 2 background = default (white)")
    else
        colour_message+=("Column 2 background = $third (${colour_names[$third - 1]})")
    fi
    if [[ ! $forth =~ ^[1-6] ]]; then 
        forth="2"  # red
        colour_message+=("Column 2 font colour = default (red)")
    else
        colour_message+=("Column 2 font colour = $forth (${colour_names[$forth - 1]})")
    fi
    colour_category="\e[${bg_color_val_names[$first - 1]};${font_color_val_names[$second - 1]}m"
    colour_data="\e[${bg_color_val_names[$third - 1]};${font_color_val_names[$forth - 1]}m"
}