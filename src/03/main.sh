#!/bin/bash
source ./get_info.sh
source ./print_info.sh
source ./choose_colour.sh

if [[ $# -ne 0 ]]; then
    echo "You have to enter exactly 0 arguments"
    exit 1
else
    get_info
    choose_colour
    print_info
fi
echo
printf "%s\n" "${colour_message[@]}"
exit 0