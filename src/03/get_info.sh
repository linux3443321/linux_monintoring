#!/bin/bash

get_info() {
INFO=( "hostname = $(hostname)"
        "timezone = $(date +"UTC %-:::z")"
        "user = $(whoami)"
        "os = $(lsb_release -d | cut -f2)"
        "date = $(date +"%d %B %Y %T")"
        "uptime = $(uptime -p)"
        "uptime_sec = $(cat /proc/uptime | awk '{print $1}')"
        "ip = $(hostname -I | awk '{print $1}')"
        "mask = $(ip -o -f inet addr show | awk '/scope global/ {print $4}')"
        "ram_total =  $(free -m | awk '/Mem:/ {printf "%.3f", $2/1000}')"
        "ram_used =  $(free -m | awk '/Mem:/ {printf "%.3f", $3/1000}')"
        "ram_free =  $(free -m | awk '/Mem:/ {printf "%.3f", $4/1000}')"
        "space_root = $(df -m / | awk '/\// {printf "%.2f", $2/1000}')"
        "space_root_used = $(df -m / | awk '/\// {printf "%.2f", $3/1000}')"
        "space_root_free = $(df -m / | awk '/\// {printf "%.2f", $4/1000}')" )
}