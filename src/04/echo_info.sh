#!/bin/bash

echo_info() {
    echo "Total number of folders (including nesting ones) = $(find $1 -type d | wc -l)"
    echo "TOP 5 folders of maximum size :" 
    echo "$(du -h $1* | sort -rh | head -n 5 | awk '{print NR " - " $2 ", " $1}')"
    echo "Total number of files = $(find $1 -type f | wc -l)"
    echo "Number of:"
    echo "Configuration files (with the .conf extension) = $(find $1 -name "*.conf" | wc -l)"
    echo "Text files = $(find $1 -name "*.txt" | wc -l)"
    echo "Executable files = "$(find $1 -perm /a+x | wc -l)
    echo "Log files (with the .log extension) = $(find $1 -name "*.log" | wc -l)"
    echo "Archive = "$(find $1 -name "*.zip" -o -name "*.tar" -o -name "*.tar.gz" -o -name "*.tgz" | wc -l)
    echo "Symbolic links = "$(find $1 -type l | wc -l)
    echo " TOP 10 files of maximum size arranged in descending order:"
    echo "$(find $1 -type f -exec du -h {} + | sort -rh | head -n 10 | awk '{print NR " - " $2 ", " $1 }')"
    echo " TOP 10 executable files of maximum size arranged in descending order:"
    echo "$(find $1 -type f -perm /a+x -exec ls -lh '{}' + | sort -rhk 5 | head -n 10 | awk '{print NR " - " $9 ", " $5 }')"
}