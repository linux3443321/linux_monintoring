#!/bin/bash
source ./echo_info.sh

start=$(date +"%s%N")
if [ $# -ne 1 ]; then
    echo "Usage: directory_check.sh /path/to/directory/"
    exit 1
fi
last_char="${1: -1}"
if [ "$last_char" != "/" ]; then
    echo "Error: Argument must end with a forward slash (/)"
    exit 1
fi
if ! [ -d $1 ]; then
    echo "Directory $1 not found"
    exit 1
fi

echo_info "$@"
ending=$(date +"%s%N")
runtime=$(( $ending - $start ))
echo "Script runtime = $(echo $runtime | awk '{printf "%.3f", $runtime/1000000000}') seconds"


exit 0