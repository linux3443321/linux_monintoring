#!/bin/bash
. ./get_info.sh
. ./print_info.sh
. ./create_file.sh

if [[ $# -eq 0 ]]; then
    get_info
    print_info
    read -p "Would you like to put it into file?" yn
    case $yn in
    [Yy]* ) create_file;;
    esac
else
    echo "Too many arguments"
fi